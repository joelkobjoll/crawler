<?php

die('Hola!');

use Slim\App;

require_once __DIR__ . '/../vendor/autoload.php';

session_start(); // fixme Maybe some session manager library?

$settings = require_once __DIR__ . '/../config/settings.php';

$app = new App($settings);

require_once __DIR__ . '/../config/dependencies.php';
require_once __DIR__ . '/../config/middleware.php';
require_once __DIR__ . '/../config/routes.php';

$app->run();