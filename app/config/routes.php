<?php

use Slim\Http\Request;
use Slim\Http\Response;


$app->get('/', function (Request $request, Response $response, array $args) {
    $this->logger->info("Entry / route");

    return $response;
});

$app->notFound(function () use ($app) {
    $app->response->setStatus(403);
});