#!/usr/bin/env python3.6
import os
import re
import sys
import json
import scrapy
import requests
import unicodedata
import pymysql

from itertools import chain

class movieSpider(scrapy.Spider):
    name = "movies"
    allowed_domains = ['newpct1.com']
    start_urls = ['http://www.newpct1.com/peliculas-hd']

    #Database settings
    DB_HOST = os.environ.get('DB_HOST')
    DB_PORT = os.environ.get('DB_PORT')
    DB_USER = os.environ.get('DB_USER')
    DB_PASSWD = os.environ.get('DB_PASW')
    DB_DB = os.environ.get('DB_NAME')

    # Database connection
    db_conn = pymysql.connect(host=DB_HOST, port=DB_PORT, user=DB_USER, passwd=DB_PASSWD, db=DB_DB, charset="utf8", use_unicode=True)
    db_cursor = db_conn.cursor()

    def parse(self, response):
        try:
            self.db_cursor.execute("""SELECT url FROM movies WHERE source = 'newpct'""")
            self.db_conn.commit()

            db_links = list(chain(self.db_cursor.fetchall()))
            db_links_decode = []
            [db_links_decode.append(x[0].encode('UTF-8')) for x in db_links]

            for movie in response.xpath('//ul[@class="pelilist"]//span[contains(text(), "MicroHD")]'):
                url = movie.xpath('../../a/@href').extract_first()

                if url in db_links_decode:
                    print('closing spider all indexed')
                    break                    
                else:
                    yield scrapy.Request(url, self.movie)
                
        except MySQLdb.Error as e:
            print("Error %d %s" % (e.args[0], e.args[1]))
    
    def movie(self, response):
        description = response.xpath('//div[@class="fichas-box"]//div[@class="descripcion_top"]')

        title = response.xpath('normalize-space(//h1/strong/text())').extract_first()
        title = unicodedata.normalize('NFKD', title).encode('ascii','ignore')

        title_original = description.re(r"original (.*?)<br>")
        title_original[0] if title_original else item['title']
        title_original = unicodedata.normalize('NFKD', title_original[0]).encode('ascii','ignore')

        yield {
            'url': response.xpath('//ul[@class="breadcrumbs"]/li[last()]/a/@href').extract_first(),
            'year': description.re(r'[ ].*?(\d{4})<br>')[0],
            'title': title,
            'title_original': title_original,
            'quality': 'microhd',
            'torrent':  response.xpath('//a[@class="btn-torrent"]/following::script[@type="text/javascript"]').re(r'window.location.href = (.*)')[0]
        }
