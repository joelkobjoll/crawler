#!/bin/bash

dir_name=$(dirname "$0")

scrapy runspider $dir_name/spider.py -o $dir_name/movies.json && python3.6 $dir_name/filter.py $dir_name/movies.json 
