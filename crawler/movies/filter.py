import pymysql.cursors
import transmissionrpc
import time
import json
import imdb
import sys
import os

# Global
RATING = 4
SOURCE = 'newpct'
TIME = time.strftime('%Y-%m-%d %H:%M:%S')

#Database settings
DB_HOST = os.environ.get('DB_HOST')
DB_PORT = os.environ.get('DB_PORT')
DB_USER = os.environ.get('DB_USER')
DB_PASSWD = os.environ.get('DB_PASW')
DB_DB = os.environ.get('DB_NAME')

# Database connection
db_conn = pymysql.connect(host=DB_HOST, port=DB_PORT, user=DB_USER, passwd=DB_PASSWD, db=DB_DB, charset="utf8", use_unicode=True)
db_cursor = db_conn.cursor()

# TransmissionRPC
tc = transmissionrpc.Client('192.168.1.10', port=9091)

def run_checker(scraped_movies):
    imdb_conn = imdb.IMDb()
    good_movies = []
    
    for scraped_movie in scraped_movies:
        imdb_movie = get_imdb_movie(imdb_conn, scraped_movie['title_original'])

        if imdb_movie['rating'] > RATING and int(scraped_movie['year']) >= 2000:
            scraped_movie['imdb'] = 'tt' + imdb_movie.movieID
            scraped_movie['rating'] = imdb_movie['rating']
            scraped_movie['poster'] = imdb_movie['cover']
            scraped_movie['source'] = SOURCE
            good_movies.append(scraped_movie)

    if good_movies:
        store_movie(good_movies)


def get_imdb_movie(imdb_conn, movie_name):
    results = imdb_conn.search_movie(movie_name)
    movie = results[0]
    imdb_conn.update(movie)

    print("{title} => {rating}".format(**movie))

    return movie


def store_movie(movies):
    for movie in movies:
        try:
            db_cursor.execute("""INSERT INTO movies(imdb, title, title_original, year, poster, url, rating, torrent, source) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)""", (movie['imdb'], movie['title'], movie['title_original'], movie['year'], movie['poster'], movie['url'], movie['rating'], movie['torrent'], movie['source']))
            db_conn.commit()

            print("ADDED => {title_original}".format(**movie))
            torrent = tc.add_torrent(movie['torrent'].replace('"', ''))
            db_cursor.execute("""UPDATE movies SET hash=%s WHERE imdb=%s""", (torrent.hashString, movie['imdb']))
            db_conn.commit()
            pass

        except:
            print("DUPLICATED => {title_original}".format(**movie))
            pass


if __name__ == '__main__':
    movies_json_file = sys.argv[1]

    with open(movies_json_file) as scraped_movies_file:
        movies = json.loads(scraped_movies_file.read())
        os.remove('movies.json')

    if movies:
        run_checker(movies)
