import os
import sys
import shutil
import transmissionrpc
import pymysql.cursors

#Directories
MOVIE_DIR = '/movies/'

TR_TORRENT_NAME = sys.argv[1]
TR_TORRENT_DIR = sys.argv[2]
TR_TORRENT_HASH = sys.argv[3]
TR_TORRENT_ID = sys.argv[4]

#Database settings
DB_HOST = os.environ.get('DB_HOST', 'localhost')
DB_PORT = os.environ.get('DB_PORT', '3306')
DB_USER = os.environ.get('DB_USER', 'root')
DB_PASSWD = os.environ.get('DB_PASW', '')
DB_DB = os.environ.get('DB_NAME', 'media')

# Database connection
db_conn = pymysql.connect(host=DB_HOST, port=DB_PORT, user=DB_USER, passwd=DB_PASSWD, db=DB_DB, charset="utf8", use_unicode=True)
db_cursor = db_conn.cursor()

db_cursor.execute("""SELECT title_original, year FROM movies WHERE hash=%s""", (TR_TORRENT_HASH))

# Transmission
tc = transmissionrpc.Client('192.168.1.10', port=9091)

# Movie
movie = db_cursor.fetchone()

# Remove job
tc.remove_torrent(TR_TORRENT_ID)

if movie:
    TORRENT_DIR = TR_TORRENT_DIR + '/' + TR_TORRENT_NAME
    for file in os.listdir(TORRENT_DIR):
        if file.endswith(".mkv"):
            movie_file = os.path.join(TORRENT_DIR, file)
            shutil.move(movie_file, MOVIE_DIR + movie[0] + ' (' + movie[1] + ').mkv')
            shutil.rmtree(TORRENT_DIR)
