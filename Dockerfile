FROM centos:latest

LABEL maintainer=jamuriano@gmail.com
LABEL maintainer=kobjoll@gmail.com

RUN yum install -y  yum-utils wget

RUN yum groupinstall -y development

RUN wget https://dev.mysql.com/get/mysql57-community-release-el7-9.noarch.rpm \
 && md5sum mysql57-community-release-el7-9.noarch.rpm \
 && rpm -ivh mysql57-community-release-el7-9.noarch.rpm \
 && yum install -y mysql-server

RUN yum install -y https://centos7.iuscommunity.org/ius-release.rpm

RUN yum install -y gcc
RUN yum install -y python36u \
 && yum install -y python36u-pip \
 && yum install -y python36u-devel \
 && yum install -y openssl \
 && yum install -y git

RUN pip3.6 install pyconfig \
                pyopenssl \
		pymysql \
		scrapy \
		requests

RUN pip3.6 install git+https://github.com/alberanid/imdbpy

RUN pip3.6 install transmissionrpc

RUN mkdir /app \ 
 && mkdir /downloads

VOLUME /downloads

COPY ./crawler /app/crawler

CMD sh /app/crawler/movies/run.sh 
